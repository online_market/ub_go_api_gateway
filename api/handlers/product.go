package handlers

import (
	"context"
	"fmt"
	"online_market/ub_go_api_gateway/api/http"
	// "online_market/ub_go_api_gateway/api/models"
	"online_market/ub_go_api_gateway/genproto/user_service"
	// "online_market/ub_go_api_gateway/pkg/helper"
	"online_market/ub_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)


// CreateProduct godoc
// @ID create_Product
// @Router /product [POST]
// @Summary Create Product
// @Description  Create Product
// @Tags Product
// @Accept json
// @Produce json
// @Param profile body user_service.CreateProduct true "CreateProductRequestBody"
// @Success 200 {object} http.Response{data=user_service.Product} "GetProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateProduct(c *gin.Context) {

	var product user_service.CreateProduct

	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().Create(
		c.Request.Context(),
		&product,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetProductByID godoc
// @ID get_product_by_id
// @Router /product/{id} [GET]
// @Summary Get Product  By ID
// @Description Get Product  By ID
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Product} "UserBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductByID(c *gin.Context) {

	userID := c.Param("id")

	if !util.IsValidUUID(userID) {
		h.handleResponse(c, http.InvalidArgument, "Category id is an invalid uuid")
		return
	}

	resp, err := h.services.ProductService().GetById(
		context.Background(),
		&user_service.ProductPrimaryKey{
			Id: userID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetHProduct godoc
// @ID get_Product_list
// @Router /product [GET]
// @Summary Get Product s List
// @Description  Get Product s List
// @Tags Product
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=user_service.GetListProductResponse} "GetAllProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ProductService().GetList(
		context.Background(),
		&user_service.GetListProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdProduct godoc
// @ID update_product
// @Router /product/{id} [PUT]
// @Summary Update Product
// @Description Update Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateProduct true "UpdateProductRequestBody"
// @Success 200 {object} http.Response{data=user_service.Product} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateProduct(c *gin.Context) {

	var product user_service.UpdateProduct

	product.Id = c.Param("id")

	if !util.IsValidUUID(product.Id) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().Update(
		c.Request.Context(),
		&product,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DelProduct godoc
// @ID delete_category
// @Router /product/{id} [DELETE]
// @Summary Delete Product
// @Description Delete Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.EmptyProduct} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteProduct(c *gin.Context) {

	productId := c.Param("id")

	if !util.IsValidUUID(productId) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	resp, err := h.services.ProductService().Delete(
		c.Request.Context(),
		&user_service.ProductPrimaryKey{Id: productId},
	)
	fmt.Println(resp.Info)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp.Info)
}
