package handlers

import (
	"context"
	"fmt"
	"online_market/ub_go_api_gateway/api/http"
	"online_market/ub_go_api_gateway/config"
	// "online_market/ub_go_api_gateway/api/models"
	"online_market/ub_go_api_gateway/genproto/user_service"
	// "online_market/ub_go_api_gateway/pkg/helper"
	"online_market/ub_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateOrder godoc
// @ID create_Order
// @Router /order [POST]
// @Summary Create Order
// @Description  Create Order
// @Tags Order
// @Accept json
// @Produce json
// @Param profile body user_service.CreateOrder true "CreateOrderBody"
// @Success 200 {object} http.Response{data=user_service.Order} "GetOrderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateOrder(c *gin.Context) {

	var order user_service.CreateOrder

	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().Create(
		c.Request.Context(),
		&order,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetOrderByID godoc
// @ID get_ordert_by_id
// @Router /order/{id} [GET]
// @Summary Get Order  By ID
// @Description Get Order  By ID
// @Tags Order
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Order} "OrderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOrderByID(c *gin.Context) {

	userID := c.Param("id")

	if !util.IsValidUUID(userID) {
		h.handleResponse(c, http.InvalidArgument, "Order id is an invalid uuid")
		return
	}

	resp, err := h.services.OrderService().GetById(
		context.Background(),
		&user_service.OrderPrimaryKey{
			Id: userID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetOrderByUserID godoc
// @Security ApiKeyAuth
// @ID get_ordertUser_by_id
// @Router /order_user [GET]
// @Summary Get Order  By User ID
// @Description Get Order User By ID
// @Tags User
// @Accept json
// @Produce json
// @Success 200 {object} http.Response{data=user_service.Order} "OrderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOrderByUserID(c *gin.Context) {

	if h.cfg.Environment == config.TestMode {
		h.log.Debug("BasicAuthMiddelware passed... testMode")
		c.Next()
		return
	}

	var auth HasAccessModel
	ok := h.hasAccess(c, &auth)
	if !ok {
		c.Abort()
		return
	}

	c.Request.Header.Add("user_id", auth.Id)
	c.Request.Header.Add("role_id", auth.RoleId)

	userID := auth.Id

	c.Next()

	if !util.IsValidUUID(userID) {
		h.handleResponse(c, http.InvalidArgument, "Order id is an invalid uuid")
		return
	}

	resp, err := h.services.OrderService().GetByUserId(
		context.Background(),
		&user_service.UserPrimaryKey{
			Id: userID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetOrder godoc
// @ID get_Order_list
// @Router /order [GET]
// @Summary Get Order  List
// @Description  Get Order  List
// @Tags Order
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=user_service.GetListOrderResponse} "GetAllOrdernseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOrderList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.OrderService().GetList(
		context.Background(),
		&user_service.GetListOrderRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdOrder godoc
// @ID update_order
// @Router /order/{id} [PUT]
// @Summary Update Order
// @Description Update Order
// @Tags Order
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateOrder true "UpdateProductOrderestBody"
// @Success 200 {object} http.Response{data=user_service.Product} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateOrder(c *gin.Context) {

	var order user_service.UpdateOrder

	order.Id = c.Param("id")

	if !util.IsValidUUID(order.Id) {
		h.handleResponse(c, http.InvalidArgument, "Order id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.OrderService().Update(
		c.Request.Context(),
		&order,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DelOrdert godoc
// @ID delete_order
// @Router /order/{id} [DELETE]
// @Summary Delete Order
// @Description Delete Order
// @Tags Order
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.EmptyOrder} "Order data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteOrder(c *gin.Context) {

	orderId := c.Param("id")

	if !util.IsValidUUID(orderId) {
		h.handleResponse(c, http.InvalidArgument, "order id is an invalid uuid")
		return
	}

	resp, err := h.services.OrderService().Delete(
		c.Request.Context(),
		&user_service.OrderPrimaryKey{Id: orderId},
	)
	fmt.Println(resp.Info)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp.Info)
}
