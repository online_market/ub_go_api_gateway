package handlers

import (
	"online_market/ub_go_api_gateway/api/http"
	"online_market/ub_go_api_gateway/config"
	"online_market/ub_go_api_gateway/genproto/auth_service"
	"online_market/ub_go_api_gateway/genproto/user_service"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// Sign In godoc
// @ID Sign_in
// @Router /sign_in [POST]
// @Summary Sign In
// @Description  Sign In
// @Tags Auth
// @Accept json
// @Produce json
// @Param login body auth_service.LoginRequest true "LoginRequestBody"
// @Success 200 {object} http.Response{data=auth_service.LoginReponse} "GetLoginBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Login(c *gin.Context) {

	var login auth_service.LoginRequest

	err := c.ShouldBindJSON(&login)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SessionService().Login(
		c.Request.Context(),
		&login,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// Sign Up godoc
// @ID Sign Up
// @Router /sign_up [POST]
// @Summary Sign Up
// @Description  Sign Up
// @Tags Auth
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateUser true "CreateUserRequestBody"
// @Success 200 {object} http.Response{data=user_service.User} "GetUserBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateUser(c *gin.Context) {

	var user user_service.CreateUser

	var id = uuid.New()

	err := c.ShouldBindJSON(&user)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	if config.PlatformWebID == c.GetHeader("Platform-Id") {
		user.Roleid = config.RoleClient
	}

	_, err = h.services.AuthService().Create(
		c.Request.Context(),
		&auth_service.CreateUser{
			Id:       id.String(),
			Username: user.FristName,
			Login:    user.Login,
			Password: user.Password,
			RoleId:   user.Roleid,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	resp, err := h.services.UserService().Create(
		c.Request.Context(),
		&user_service.CreateUser{
			Id:        id.String(),
			FristName: user.FristName,
			LastName:  user.LastName,
			Balance:   user.Balance,
			Type:      user.Type,
			Login:     user.Login,
			Password:  user.Password,
			Roleid:    user.Roleid,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}
