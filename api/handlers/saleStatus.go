package handlers

import (
	"context"
	"online_market/ub_go_api_gateway/api/http"
	"online_market/ub_go_api_gateway/genproto/user_service"
	"online_market/ub_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateSaleStatus godoc
// @ID create_SaleStatus
// @Router /saleStatus [POST]
// @Summary Create SaleStatus
// @Description  Create SaleStatus
// @Tags SaleStatus
// @Accept json
// @Produce json
// @Param profile body user_service.CreateSaleStatus true "CreateSaleStatusBody"
// @Success 200 {object} http.Response{data=user_service.SaleStatus} "GetSaleStatusBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSaleStatus(c *gin.Context) {

	var order user_service.CreateSaleStatus

	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleStatusService().CreateStatus(
		c.Request.Context(),
		&order,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSaleStatus godoc
// @ID get_SaleStatus_list
// @Router /saleStatus [GET]
// @Summary Get SaleStatus  List
// @Description  Get SaleStatus  List
// @Tags SaleStatus
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=user_service.GetListSaleStatusResponse} "GetAllSaleStatusnseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleStatusList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SaleStatusService().GetListStatus(
		context.Background(),
		&user_service.GetListSaleStatusRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdSaleStatus godoc
// @ID update_saleStatus
// @Router /saleStatus/{id} [PUT]
// @Summary Update SaleStatus
// @Description Update SaleStatus
// @Tags SaleStatus
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.CreateSaleStatus true "UpdateProductSaleStatusestBody"
// @Success 200 {object} http.Response{data=user_service.SaleStatus} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSaleStatus(c *gin.Context) {

	var order user_service.CreateSaleStatus

	order.Id = c.Param("id")

	if !util.IsValidUUID(order.Id) {
		h.handleResponse(c, http.InvalidArgument, "SaleStatus id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&order)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleStatusService().UpdateStatus(
		c.Request.Context(),
		&order,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}
