package services

import (
	"online_market/ub_go_api_gateway/config"
	"online_market/ub_go_api_gateway/genproto/auth_service"
	"online_market/ub_go_api_gateway/genproto/user_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	UserService() user_service.UsersClient
	CategoryService() user_service.CategorysClient
	ProductService() user_service.ProductsClient
	OrderService() user_service.OrdersClient
	AuthService() auth_service.AuthServiceClient
	SessionService() auth_service.SessionServiceClient
	SaleStatusService()  user_service.SaleStatusesClient
}

type grpcClients struct {
	userService     user_service.UsersClient
	categoryService user_service.CategorysClient
	productService  user_service.ProductsClient
	orderService    user_service.OrdersClient
	authService     auth_service.AuthServiceClient
	sessionService auth_service.SessionServiceClient
	saleStatus user_service.SaleStatusesClient

}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Auth Service...
	connAuthService, err := grpc.Dial(
		cfg.AuthServiceHost+cfg.AuthGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		authService:     auth_service.NewAuthServiceClient(connAuthService),
		userService:     user_service.NewUsersClient(connUserService),
		categoryService: user_service.NewCategorysClient(connUserService),
		orderService:    user_service.NewOrdersClient(connUserService),
		productService:  user_service.NewProductsClient(connUserService),
		sessionService: auth_service.NewSessionServiceClient(connAuthService),
		saleStatus: user_service.NewSaleStatusesClient(connUserService),

	}, nil
}

func (g *grpcClients) UserService() user_service.UsersClient {
	return g.userService
}

func (g *grpcClients) CategoryService() user_service.CategorysClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() user_service.ProductsClient {
	return g.productService
}


func (g *grpcClients) OrderService() user_service.OrdersClient {
	return g.orderService
}

func (g *grpcClients) SaleStatusService() user_service.SaleStatusesClient {
	return g.saleStatus
}

// Auth Service...

func (g *grpcClients) AuthService() auth_service.AuthServiceClient {
	return g.authService
}

func (g *grpcClients) SessionService() auth_service.SessionServiceClient {
	return g.sessionService
}


