package handlers

import (
	"context"
	"fmt"
	"online_market/ub_go_api_gateway/api/http"
	// "online_market/ub_go_api_gateway/api/models"
	"online_market/ub_go_api_gateway/genproto/user_service"
	// "online_market/ub_go_api_gateway/pkg/helper"
	"online_market/ub_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateCategory godoc
// @ID create_category
// @Router /category [POST]
// @Summary Create Category
// @Description  Create Category
// @Tags Category
// @Accept json
// @Produce json
// @Param profile body user_service.CreateCategory true "CreateCategoryRequestBody"
// @Success 200 {object} http.Response{data=user_service.Category} "GetCategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCategory(c *gin.Context) {

	var hobbi user_service.CreateCategory

	err := c.ShouldBindJSON(&hobbi)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CategoryService().Create(
		c.Request.Context(),
		&hobbi,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetCategoryByID godoc
// @ID get_category_by_id
// @Router /category/{id} [GET]
// @Summary Get Category  By ID
// @Description Get Category  By ID
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Category} "UserBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCategoryByID(c *gin.Context) {

	userID := c.Param("id")

	if !util.IsValidUUID(userID) {
		h.handleResponse(c, http.InvalidArgument, "Category id is an invalid uuid")
		return
	}

	resp, err := h.services.CategoryService().GetById(
		context.Background(),
		&user_service.CategoryPrimaryKey{
			Id: userID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetHobbiList godoc
// @ID get_category_list
// @Router /category [GET]
// @Summary Get Category s List
// @Description  Get Category s List
// @Tags Category
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=user_service.GetListCategoryResponse} "GetAllCategoryResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCategoryList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.CategoryService().GetList(
		context.Background(),
		&user_service.GetListCategoryRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateHobbi godoc
// @ID update_category
// @Router /category/{id} [PUT]
// @Summary Update Category
// @Description Update Category
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateCategory true "UpdateCategoryRequestBody"
// @Success 200 {object} http.Response{data=user_service.Category} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCategory(c *gin.Context) {

	var hobbi user_service.UpdateCategory

	hobbi.Id = c.Param("id")

	if !util.IsValidUUID(hobbi.Id) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&hobbi)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CategoryService().Update(
		c.Request.Context(),
		&hobbi,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteHobbi godoc
// @ID delete_category
// @Router /category/{id} [DELETE]
// @Summary Delete Category
// @Description Delete Category
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.EmptyCategory} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCategory(c *gin.Context) {

	categoryId := c.Param("id")

	if !util.IsValidUUID(categoryId) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	resp, err := h.services.CategoryService().Delete(
		c.Request.Context(),
		&user_service.CategoryPrimaryKey{Id: categoryId},
	)
	fmt.Println(resp.Info)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp.Info)
}
